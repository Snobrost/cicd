import  { useState,useEffect } from "react";


const Products = () =>{
  const url = 'https://fakestoreapi.com/products'
  const useFetch = (url) => {
    const [state,setState] = useState({
      items: [],
      loading: true,
    })

    useEffect(() => {
      if(state.loading){
        (async () => {
          const res = await fetch(url)
          const data = await res.json()
          if (res.ok) {
            setState({
              items: data,
              loading: false,
            })
          } else {
            console.log(JSON.stringify(data))
            setState({
              items: [],
              loading: false,
            })
          }
        })()
      }
      
    }) 
    return [
      state.loading,
      state.items,
    ]
  }
  return(useFetch(url))

}

  export default Products
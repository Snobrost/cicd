import React from 'react'
import ItemCart from './ItemCart'
/**/

const Cart = ({ productsCart,handleChangeshow,nbElement ,prixTotal,handleChangeAdd,handleChangesEmpty}) => (
  <div >
    <div>Cart {nbElement}</div><br/>
    
    {nbElement===0?<div>Empty</div>: 
      <div>
        {productsCart.map(product =>
          <ItemCart
            key={product[0]}
            title={product[0]}
            quantity={product[1]}
            price={product[2]}
            img = {product[3]}
            handleChangeAdd={handleChangeAdd}
          />
        )}
        <button id="button-cart-clear" onClick={handleChangesEmpty}>Empty cart</button>
        <button id="button-cart-end" onClick={handleChangeshow}>Checkout {prixTotal} $</button>
      </div>}
      <button id="button-cart-close" onClick={handleChangeshow}>Close</button>
  </div>
)

export default Cart
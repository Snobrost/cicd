

const ItemCart = ({title,img,quantity,price,handleChangeAdd}) =>{
    if(quantity>0){
        return(
            <div id ="product_cart">
                <div  >
                    <img id ="cart-image" src = {img} alt="Profil"/>
                </div>
                <div id="product-title_cart">
                    {title}
                </div>
                <div id="quantity">
                    <button id="add" type="button"  qty={-1} title={title} price={price} img= {img} onClick={handleChangeAdd}>
                        -
                    </button>  
                    <div id="product-quantity_cart">
                        &nbsp;{quantity}&nbsp;
                    </div>
                    <button id="add" type="button"  qty={1} title={title} price={price} img= {img} onClick={handleChangeAdd}>
                        +
                    </button>
                </div><br/>
                <div id="product-price_cart">
                    Individual price {price}$
                </div>  
                <div id="product-pricettc_cart">
                    Total {price*quantity}$
                </div>       
            </div>
        )
    }else{
    return(<div>{quantity}</div>)
    }
}

export default ItemCart